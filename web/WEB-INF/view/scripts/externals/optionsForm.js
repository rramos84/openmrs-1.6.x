//optionsForm.js

jQuery(document).ready(function(){
	//Obtenemos el formulario completo de nombres y pass
	//Seleccionamos los inputs que nos interesa mostrar 
	//y luego los eliminamos para poder mostrarlos en el formulario que crearemos
	var error_oldPassword = jQuery("#error_oldPassword").html();
	var error_newPassword	=	jQuery("#error_newPassword").html();	
	var error_confirmPassword	=	jQuery("#error_confirmPassword").html();	

		console.log(error_oldPassword);
		console.log(error_newPassword);
		console.log(error_confirmPassword);

		var givenName		=	jQuery("input[name='personName.givenName']");
				givenName.remove();
		var middleName		=	jQuery("input[name='personName.middleName']");
				middleName.remove();
		var familyName		=	jQuery("input[name='personName.familyName']");
				familyName.remove();
		var familyName2		=	jQuery("input[name='personName.familyName2']");
				familyName2.remove();
		var oldPassword		=	jQuery("input[name='oldPassword']");
				oldPassword.remove();
		var newPassword		=	jQuery("input[name='newPassword']");
				newPassword.remove();
		var confirmPassword	=	jQuery("input[name='confirmPassword']");
				confirmPassword.remove();
		var email_address	=	jQuery("input[name='notificationAddress']");
				email_address.remove();
				val_email_address = email_address.val();
		
		
			if(val_email_address=='' || val_email_address==null || val_email_address=='undefined'){
				val_email_address = '';
			}
			
		var enviar_form		=	jQuery("#enviar_form");
			enviar_form.remove();

			
			var form_completo = jQuery("#options_login_form").html();
			var optionsForm = jQuery("#optionsForm");
			//ocultamos todos los formularios
			optionsForm.hide();
				
		//agregamos la vista del formulario nuevo
		var div_agregar	=	'<div id="mi_perfil"><h3>Mi Perfil</h3></div>';
		
		//Insertamos el div nuevo al principio del formulario
		jQuery("#optionsForm").parent("form").append(div_agregar);
		//Insertamos el boton de envio de formulario
		
		//agregamos dentro del div una tabla con el contenido del formulario a mostrar
		jQuery("#mi_perfil").append("<table id='form_completo'>"+form_completo+"</table>");

			
		//Extraemos los td de formulario a mostrar y los asignamos a una variable
		//Nombre
		var td_nombre_txt		=	jQuery("#form_completo tbody tr:nth-child(2) td:nth-child(1)").html();
				givenName.attr("title", td_nombre_txt);
		var td_nombre			=	jQuery("#form_completo tbody tr:nth-child(2) td:nth-child(2)").html();
		//Segundo Nombre
		var td_seg_nombre_txt	=	jQuery("#form_completo tbody tr:nth-child(3) td:nth-child(1)").html();
				middleName.attr("title", td_seg_nombre_txt);
		var td_seg_nombre		=	jQuery("#form_completo tbody tr:nth-child(3) td:nth-child(2)").html();
		//Apellido 1
		var td_apellido1_txt	=	jQuery("#options_login_form tbody tr:nth-child(4) td:nth-child(3)").html();
				familyName.attr("title", td_apellido1_txt);
		var td_apellido1		=	jQuery("#options_login_form tbody tr:nth-child(4) td:nth-child(4)").html();
		//Apellido 2
		var td_apellido2_txt	=	jQuery("#options_login_form tbody tr:nth-child(4) td:nth-child(5)").html();
				familyName2.attr("title", td_apellido2_txt);
				email_address.attr("title", "Email");
		var td_apellido2		=	jQuery("#options_login_form tbody tr:nth-child(4) td:nth-child(6)").html();
		//Password Antiguo
		var td_pss_old_txt	=	jQuery("#form_completo tbody tr:nth-child(7) td:nth-child(1)").html();
		var td_pss_old		=	jQuery("#form_completo tbody tr:nth-child(7) td:nth-child(2)").html();
		//Password Nuevo
		var td_pss_new_txt	=	jQuery("#form_completo tbody tr:nth-child(8) td:nth-child(1)").html();
		var td_pss_new		=	jQuery("#form_completo tbody tr:nth-child(8) td:nth-child(2)").html();
		//Confirmar Nuevo Password
		var td_pss_rpt_txt	=	jQuery("#form_completo tbody tr:nth-child(9) td:nth-child(1)").html();
		var td_pss_rpt		=	jQuery("#form_completo tbody tr:nth-child(9) td:nth-child(2)").html();

		
		//eliminamos por completo la tabla para facilitar la implementacion del nuevo dise�o
		$("#form_completo").remove();
		//ocultamos las columnas y filas que no deseamos mostrar en el nuevo form
		jQuery(".hide_js").hide();
		
	
		//agregamos el nuevo estilo de dise�o del formulario mi perfil
		jQuery("#mi_perfil").append(
				"<div class='center'>" +
					"<div class='left'>" +
						"<table width='80%'>" +
							"<tr id='tr_nombre'>" +
								"<td>"+td_nombre_txt+"</td>" +
								"<td>"+td_nombre+" "+td_seg_nombre+"</td>" +
							"</tr>" +
							"<tr id='tr_apellido'>"+
								"<td>"+td_apellido1_txt+"</td>"+
								"<td>"+td_apellido1+" "+td_apellido2+"</td>"+
							"</tr><tr id='tr_email'>" +
								"<td> Email</td>" +
								"<td> <span id='txt_email'>"+val_email_address+"</span></td>" +
							"</tr>" +
						"</table>" +
					"</div>" +
					"<div class='right'>" +
						"<table>" +
							"<tr>"+
								"<td>"+td_pss_old_txt+"</td>" +
								"<td id='td_oldPassword'></td>" +
							"</tr><tr>"+
								"<td>"+td_pss_new_txt+"</td>" +
								"<td id='td_newPassword'></td>" +
							"</tr><tr>"+
								"<td>"+
								td_pss_rpt_txt+
								"</td>" +
								"<td id='td_confirmPassword'></td>" +
							"</tr>"+
							"<tr><td colspan='2' id='error_log'></td></tr>" + 
						"</table>" +
					"</div>" +
				"</div>"+
				"<div class='mod_inp'><a id='modificar_inputs'>Modificar</a></div>"
		);
		
		//Adjuntamos los errores a la tabla
			if(error_oldPassword!=null){
				jQuery("#error_log").append("<div class='error' style='width:100%;'>"+error_oldPassword+"</div>");
			}
			if(error_newPassword!=null){
				jQuery("#error_log").append("<div class='error' style='width:100%;'>"+error_newPassword+"</div>");
			}
			if(error_confirmPassword!=null){
				jQuery("#error_log").append("<div class='error' style='width:100%;'>"+error_confirmPassword+"</div>");
			}
		
		
		
			//insertamos los inputs de passwords
			jQuery("#td_oldPassword").append(oldPassword);
			//td_newPassword
			jQuery("#td_newPassword").append(newPassword);
			//td_confirmPassword
			jQuery("#td_confirmPassword").append(confirmPassword);

			//Extraemos los valores de los inputs para poder mostrarlos como texto, con sus respectivas validaciones.
			val_nombre	= 	givenName.val();
			if(val_nombre=='undefined' || val_nombre=='' || val_nombre==null){
				val_nombre='';
			}
			val_seg_nom	=	middleName.val();
			if(val_seg_nom=='undefined' || val_seg_nom=='' || val_seg_nom==null){
				val_seg_nom='';
			}
			val_apellido=	familyName.val();
			if(val_apellido=='undefined' || val_apellido=='' || val_apellido==null){
				val_apellido='';
			}
			val_apellido2=	familyName2.val();
			if(val_apellido2=='undefined' || val_apellido2=='' || val_apellido2==null){
				val_apellido2='';
			}
		
			//Mostramos los valores en las tablas seleccionadas
			//asignamos un estilo a las tablas
			jQuery(".left tr td:first-child").css("width","40%");
			///.left tr td:last-child
			jQuery(".left tr td:last-child").css("width","60%");
			//Mostramos los valores expresados en textos segun la imagen 3.2
			jQuery("#tr_nombre td:last-child").prepend("<span id='txt_nombre'>"+val_nombre+"</span>");
			jQuery("#tr_nombre td:last-child").append("<span id='txt_seg_nom'>"+val_seg_nom+"</span>");
			jQuery("#tr_apellido td:last-child").prepend("<span id='txt_apellido'>"+val_apellido+"</span>");
			jQuery("#tr_apellido td:last-child").append("<span id='txt_apellido2'>"+val_apellido2+"</span>");
		
			//var userAgent = navigator.userAgent.toLowerCase();
			//jQuery.browser = {
			//	version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
			//	chrome: /chrome/.test( userAgent ),
			//	safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),
			//	opera: /opera/.test( userAgent ),
			//	msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
			//	mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ),
				
				
			//}
			//if(jQuery.browser.msie==true){
				//jQuery(".right tr:first-child ").remove();
			//}
			
			//console.log(jQuery.browser.version);
			
			givenName.attr("size","10");
			middleName.attr("size","10");
			familyName.attr("size","10");
			familyName2.attr("size","10");
			email_address.attr("size","24");
			
			
			// Nueva Propuesta visual
			
			
	
				//Dejamos los inputs al lado y ocultos &nbsp;
				jQuery("#tr_nombre td:last-child").append(givenName.css({"opacity" : "0","display":"none"}));
				jQuery("#tr_nombre td:last-child").append("&nbsp;&nbsp;");
				jQuery("#tr_nombre td:last-child").append(middleName.css({"opacity" : "0","display":"none"}));
				jQuery("#tr_apellido td:last-child").append(familyName.css({"opacity" : "0","display":"none"}));
				jQuery("#tr_apellido td:last-child").append("&nbsp;&nbsp;");
				jQuery("#tr_apellido td:last-child").append(familyName2.css({"opacity" : "0","display":"none"}));
				jQuery("#tr_email td:last-child").append(email_address.css({"opacity" : "0","display":"none"}));
				
				
				jQuery("#modificar_inputs").one("click",function(){
					
					jQuery("#txt_nombre, #txt_seg_nom").animate({opacity:0},200,function(){
						// al ternimar la animacion comenzara la otra
						jQuery(this).css("display","none");
						givenName.css("display","").animate({opacity:1},200);
						middleName.css("display","").animate({opacity:1},200);
					});
					
					jQuery("#txt_apellido,#txt_apellido2").animate({opacity:0},200,function(){
						// al ternimar la animacion comenzara la otra
						jQuery(this).css("display","none");
						familyName.css("display","").animate({opacity:1},200);
						familyName2.css("display","").animate({opacity:1},200);
						
					});
					
					jQuery("#txt_email").animate({opacity:0},200,function(){
						// al ternimar la animacion comenzara la otra
						jQuery(this).css("display","none");
						email_address.css("display","").animate({opacity:1},200);
					});
					
				});
		
			
	
		//para el final
		jQuery("#mi_perfil").append(enviar_form);
		jQuery("#mi_perfil").corner("keep 10px");
		jQuery("#mi_perfil h3").corner("top 6px");
		//jQuery("#optionsForm input, #optionsForm select").attr("disabled","disabled");
});
